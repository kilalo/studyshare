import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TokenService } from './services/token.service'
import { AuthService } from './services/auth.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Study Share';
  @Input() id: number;

  users : any[];

  
 constructor(
 	private token: TokenService, 
 	private authService:AuthService, 
 	private router:Router) {
 	if (this.token.loggedIn() && window.location.pathname == '/') {
      router.navigate(['home']);
    }
 }


}
