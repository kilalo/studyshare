import { BrowserModule } from '@angular/platform-browser';
import { NgModule} from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import 'hammerjs';

import { AppComponent } from './app.component';
import { Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ProfileComponent } from './components/profile/profile.component';
import { RequestResetComponent } from './components/password/request-reset/request-reset.component';
import { ResponseResetComponent } from './components/password/response-reset/response-reset.component';
import { SignupComponent } from './components/signup/signup.component';
import { UserComponent } from './components/user/user.component';



import { JarwisService } from './services/jarwis.service';
import { TokenService } from './services/token.service';
import { AuthService } from './services/auth.service';
import { BeforeLoginService } from './services/before-login.service';
import { AfterLoginService } from './services/after-login.service';
import { UserService } from './services/user.service';
import { PostService } from './services/post.service';


import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
    MatDatepickerModule,
    MatNativeDateModule,
    MatInputModule,
    MatButtonModule,
    MatPaginator,
    MatTableModule,
    MatSort, 
    MatProgressSpinnerModule, 
    MatPaginatorModule, 
    MatSortModule, 
    MatMenuModule, 
    MatSelectModule,
    MatCardModule,
    MatRadioModule,
    MatCheckboxModule

} from '@angular/material';
import { CountToModule } from 'angular-count-to';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core'
import {PostComponent} from "./components/post/post.component";
import { CommentComponent } from './components/comment/comment.component';
import { PostListComponent } from './components/post-list/post-list.component';
import { AddPostFormComponent } from './components/add-post-form/add-post-form.component';
import { FooterComponent } from './components/footer/footer.component';




const appRoutes: Routes = [
  { path: 'home', component: HomeComponent, canActivate : [AfterLoginService] },
  // { path: '', component: HomeComponent, canActivate : [AfterLoginService] },
  { path: '', component: LoginComponent, canActivate : [BeforeLoginService] },
  { path: 'login', component: LoginComponent, canActivate : [BeforeLoginService] },
  { path: 'signup', component: SignupComponent, canActivate : [BeforeLoginService] },
  { path: 'request-password-reset', component: RequestResetComponent, canActivate : [BeforeLoginService] },
  { path: 'response-password-reset', component: ResponseResetComponent, canActivate : [BeforeLoginService] },
  { path: 'profile/:id', component: ProfileComponent, canActivate : [AfterLoginService] },
  { path: 'post/:id', component: PostComponent, canActivate : [AfterLoginService] },
  { path: 'post-list', component: PostListComponent, canActivate : [AfterLoginService] },
  { path: 'add-post', component: AddPostFormComponent, canActivate : [AfterLoginService] }
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PostComponent,
    LoginComponent,
    NavbarComponent,
    ProfileComponent,
    RequestResetComponent,
    ResponseResetComponent,
    SignupComponent,
    UserComponent,
    CommentComponent,
    PostListComponent,
    AddPostFormComponent,
    FooterComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatButtonModule,
    MatTableModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatSortModule,
    MatSelectModule,
    MatCardModule,
    MatRadioModule,
    MatCheckboxModule,
    CountToModule

  ],
  providers: [
  JarwisService,
  TokenService,
  AuthService,
  BeforeLoginService,
  AfterLoginService,
  UserService,
  PostService,
  MatDatepickerModule,
  {provide: MAT_DATE_LOCALE, useValue: 'fr-FR'},
      MatPaginator,
      MatSort
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
