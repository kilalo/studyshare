import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private http:HttpClient) { }

  private baseUrl = 'http://api-study-share.com/api';

  getPostById(id: number) {
  	return this.http.get(this.baseUrl + '/posts/' + id);
  }

  getCommentsByPostId(id: number) {
  	return this.http.get(this.baseUrl + '/posts/' + id + '/comments');
  }
}
