import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class JarwisService {
 
  private baseUrl = 'http://api-study-share.com/api';

  
  constructor(
    private http:HttpClient
    ) { }

  signup(data){
  	return this.http.post(this.baseUrl.concat('/signup'),data);
  }

  login(data){
  	return this.http.post(this.baseUrl.concat('/login'),data);
  }

  sendPasswordResetLink(data){
  	return this.http.post(this.baseUrl.concat('/sendPasswordResetLink'),data);
  }

  changePassword(data){
  	return this.http.post(this.baseUrl.concat('/resetPassword'),data);
  }

  addPost(data){
    return this.http.post(this.baseUrl.concat('/posts/register'),data);
  }

  addComment(data){
    return this.http.post(this.baseUrl.concat('/comments/register'),data);
  }

  addLikePost(data){
    return this.http.post(this.baseUrl.concat('/posts/like'),data);
  }
  addLikeComment(data){
    return this.http.post(this.baseUrl.concat('/comments/like'),data);
  }

  getAllPosts(){
    return this.http.get(this.baseUrl.concat('/tableposts'));
  }

  getAllCourses(){
    return this.http.get(this.baseUrl.concat('/courses'));
  }
  getAllDomains(){
    return this.http.get(this.baseUrl.concat('/domains'));
  }
  getCoursesByDomainId(domain_id){
    return this.http.get(this.baseUrl.concat('/courses/domains/'+domain_id));
  }
  getDomainsByCollegeId(college_id){
    return this.http.get(this.baseUrl.concat('/domains/colleges/'+college_id));
  }
  getAllColleges(){
    return this.http.get(this.baseUrl.concat('/colleges'));
  }

  getUserPosts(id){
    return this.http.get(this.baseUrl.concat('/posts/get_user/'+id));
  }

  getUserLike(data){
    return this.http.get(this.baseUrl.concat('/is_post_liked'),data);
  }

  isPostLiked(data){
      return this.http.post(this.baseUrl.concat('/likes/is_post_liked'),data);
  }
  isCommentLiked(data){
      return this.http.post(this.baseUrl.concat('/likes/is_comment_liked'),data);
  }

  getLikeStatus(data){
      return this.http.post(this.baseUrl.concat('/likes/get_status'),data);
  }
  
}


