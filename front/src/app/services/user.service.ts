import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private baseUrl = 'http://api-study-share.com/api';

  private user = [];

  usersSubject = new Subject<any[]>();


  constructor(private http:HttpClient) { }

  emitUserSubject() {
    return this.user;
  }

  getUserById(id: number) {
  	return this.http.get(this.baseUrl + '/users/' + id);

  }


  getUser(){
  	return this.user;
  }

  handle(id){
  	localStorage.setItem('id',id);

  }

  getCurrentUserId(){
  	return parseInt(localStorage.getItem('id'));
  }

  remove(){
  	localStorage.removeItem('id');
  }
}
