import { Component, OnInit } from '@angular/core';
import { JarwisService } from '../../../services/jarwis.service';


@Component({
  selector: 'app-request-reset',
  templateUrl: './request-reset.component.html',
  styleUrls: ['./request-reset.component.css']
})
export class RequestResetComponent implements OnInit {
  public form = {

  	email:null

  };
  
  public error=null;


  constructor(
  	private Jarwis:JarwisService
  	) { }

  ngOnInit() {
  }

  onSubmit(){
  	this.Jarwis.sendPasswordResetLink(this.form).subscribe(
  		data => this.handleResponse(data),
  		error => this.handleError(error)
  	);
  }

  handleResponse(data){

  	this.form.email=null;

  }

  handleError(error){

  	this.error = error.error.error;

  }

}
