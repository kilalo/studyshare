import { Component, OnInit, Input, ElementRef, Renderer2, ViewChild } from '@angular/core';
import { JarwisService } from '../../services/jarwis.service';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-comment, post-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})
export class CommentComponent implements OnInit {
  @Input() commentText: string;
  @Input() commentUser: [];
  @Input() commentUserForename: string;
  @Input() commentFile: string;
  @Input() commentDate: Date;
  @Input() id:number;
  @Input() like_count:number;
  @ViewChild('likeCount') likeCount;
  @ViewChild('likeButton')likeButton;
  @ViewChild('dislikeButton') dislikeButton;



  ngOnInit() {
  }

  ngAfterViewInit(){
    let user_id= this.User.getCurrentUserId().toString();
    let formData = new FormData(); 
    formData.append('comment_id', this.id.toString()); 
    formData.append('user_id', user_id); 
    this.Jarwis.isCommentLiked(formData).subscribe(
      data => this.handleIsCommentLikedResponse(data)
    );
  }

  elRef: ElementRef

  constructor(
  	elRef: ElementRef,
  	private Jarwis:JarwisService,
  	private User:UserService,
  	private renderer:Renderer2
  	) {
    this.elRef = elRef;
  }      

  getHtmlContent() {
    return this.elRef.nativeElement.innerHTML;
  }

  like(data){
	  let formData = new FormData();
	  formData.append('comment_id', this.id.toString());
	  formData.append('user_id', this.User.getCurrentUserId().toString() );
    let count = 0;
    if(data == '1'){
        if(this.dislikeButton._elementRef.nativeElement.classList.contains('liked')){
            this.renderer.removeClass(this.dislikeButton._elementRef.nativeElement,'liked');
            this.renderer.addClass(this.likeButton._elementRef.nativeElement,'liked');
            count = 2;
            data = 1;
        }else if(this.likeButton._elementRef.nativeElement.classList.contains('liked')){
            this.renderer.removeClass(this.likeButton._elementRef.nativeElement,'liked');
            count = -1;
            data = 0;
        }else{
            this.renderer.addClass(this.likeButton._elementRef.nativeElement,'liked');
            count = 1;
            data = 1;
        }
    }else{
        if(this.likeButton._elementRef.nativeElement.classList.contains('liked')){
            this.renderer.removeClass(this.likeButton._elementRef.nativeElement,'liked');
            this.renderer.addClass(this.dislikeButton._elementRef.nativeElement,'liked');
            data = -1;
            count = -2;
        }else if(this.dislikeButton._elementRef.nativeElement.classList.contains('liked')){
            data = 0;
            count = 1;
            this.renderer.removeClass(this.dislikeButton._elementRef.nativeElement,'liked');
        }else{
            data = -1;
            count = -1;
            this.renderer.addClass(this.dislikeButton._elementRef.nativeElement,'liked');
        }
    }
	  formData.append('like', data);
    formData.append('count_like', count.toString());

    var like = parseInt(this.likeCount.nativeElement.innerHTML) + count;
    this.likeCount.nativeElement.innerHTML = like;

	  this.Jarwis.addLikeComment(formData).subscribe(
		  data => data
	  );
  }


  handleIsCommentLikedResponse(data){
    if(data){
      let user_id= this.User.getCurrentUserId().toString();
      let formData = new FormData(); 
      formData.append('comment_id', this.id.toString()); 
      formData.append('user_id', user_id); 
      this.Jarwis.getLikeStatus(formData).subscribe(
          data => this.handleLikeStatusResponse(data)
        );
      }
    }

  handleLikeStatusResponse(data){
     let count = data;
     if(count == 1){
       this.renderer.addClass(this.likeButton._elementRef.nativeElement,'liked');
     }else if(count == -1){
       this.renderer.addClass(this.dislikeButton._elementRef.nativeElement,'liked');
     }
  }

}
