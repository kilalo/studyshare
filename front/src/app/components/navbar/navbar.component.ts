import { Component, OnInit, Input, NgModule } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { TokenService } from '../../services/token.service';
import { UserService } from '../../services/user.service';
import {MatMenuModule, MatButtonModule} from '@angular/material';


@NgModule({
  imports: [MatMenuModule, MatButtonModule]
})

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  public loggedIn:boolean;
  @Input() id: number;


  constructor(
  	private Auth:AuthService,
  	private router:Router,
  	private Token:TokenService,
    private User:UserService,
    private route: ActivatedRoute
  	) { }

  logout(event:MouseEvent){
  	event.preventDefault();
  	this.Token.remove();
    this.User.remove();
  	this.Auth.changeAuthStatus(false);
  	this.router.navigateByUrl('/login');
  }

  updateUserId(){
    this.id = this.User.getCurrentUserId();
  }

  ngOnInit() {
  	this.Auth.authStatus.subscribe(value => this.loggedIn = value);
    this.route.params.subscribe(
        params =>{
            this.id = this.User.getCurrentUserId();
        }
    )
  }

}
