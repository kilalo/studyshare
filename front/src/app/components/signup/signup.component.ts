import { Component, OnInit, NgModule, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { JarwisService } from '../../services/jarwis.service';
import { TokenService } from '../../services/token.service';
import { AuthService } from '../../services/auth.service';
import { UserService } from '../../services/user.service';
import {MatDatepickerModule, MatNativeDateModule, MatInputModule} from '@angular/material';

@NgModule({
  imports: [MatDatepickerModule, MatInputModule],

})

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  @ViewChild('checkboxCourse') checkboxCourse;
  @ViewChild('checkboxDomain') checkboxDomain;
  public addCourse=false;
  public addDomain=false;
  public addCollege=false;
  public domains= null;
  public domain_name=null;
  public colleges=null;
  public college_name=null;
  public form = {
  	email:null,
  	name:null,
  	forename:null,
    birth_date:null,
    domain:null,
    college:null,
    city:null,
  	password:null,
  	password_confirmation:null
  };

  public error = [];

  constructor(
  	private Jarwis:JarwisService,
  	private Token:TokenService,
  	private router:Router,
    private Auth:AuthService,
    private User:UserService
  	) { }

  onSubmit(){
    let formData = new FormData(); 
    formData.append('email', this.form.email); 
    formData.append('name', this.form.name);
    formData.append('forename', this.form.forename);
    formData.append('birth_date', this.form.birth_date);
    formData.append('password', this.form.password);
    formData.append('password_confirmation', this.form.password_confirmation);
    formData.append('city', this.form.city);

    if(this.form.college.college != null){
      formData.append('college', this.form.college.name);
    }else{
      formData.append('college', this.form.college);
    }
    if(this.form.college.domain != null){
      formData.append('domain', this.form.domain.name);
    }else{
      formData.append('domain', this.form.domain);
    }

  	this.Jarwis.signup(formData).subscribe(
  		data => this.handleResponse(data),
  		error => this.handleError(error)
  	);
  }

  handleResponse(data){
  	this.Token.handle(data.access_token);
    this.User.handle(data.id);
    this.Auth.changeAuthStatus(true);
  	this.router.navigateByUrl('/home');
    this.reload();
    window.location.href = window.location.origin + '/home';
  }

  reload(){
     window.location.reload();
  }

  handleError(error){
    this.error = error.error.errors;	
  }
  ngOnInit() {
      this.Jarwis.getAllColleges().subscribe(
        data => this.handleCollegesResponse(data),
        error => this.handleCollegesError(error)
    );
  }

  handleDomainsResponse(data){
    this.domains = data.data;
  }

  handleDomainsError(error){
    this.error = error.error.error;
  }
  handleCollegesResponse(data){
    this.colleges = data.data;
  }

  handleCollegesError(error){
    this.error = error.error.error;
  }

  onCollegeChange(data) {
    this.form.city = data.value.city;
    this.college_name = data.value.name;
    this.Jarwis.getDomainsByCollegeId(data.value.id).subscribe(
      data => this.handleDomainsResponse(data),
      error => this.handleDomainsError(error)
    );
  }


  addNewCollege(){
    this.addCollege = !this.addCollege;
    this.checkboxDomain._checked = !this.checkboxDomain._checked;
    this.addDomain = !this.addDomain;


  }

  addNewDomain(){
    this.addDomain = !this.addDomain;

  }


}
