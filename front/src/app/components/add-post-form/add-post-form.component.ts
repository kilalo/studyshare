import { Component, OnInit, NgModule,ViewChild } from '@angular/core';
import {MatFormFieldModule, MatSelectModule, MatCardModule, MatButtonModule } from '@angular/material';
import { Router } from '@angular/router';
import { JarwisService } from '../../services/jarwis.service';
import { UserService } from '../../services/user.service';



@NgModule({
  imports: [MatFormFieldModule, MatSelectModule, MatCardModule, MatButtonModule]
})
@Component({
  selector: 'app-add-post-form',
  templateUrl: './add-post-form.component.html',
  styleUrls: ['./add-post-form.component.css']
})
export class AddPostFormComponent implements OnInit {

  @ViewChild('fileInput') fileInput;
  @ViewChild('checkboxCourse') checkboxCourse;
  @ViewChild('checkboxDomain') checkboxDomain;


  public name: string;
  public id: number;
  public error= null;
  public domains= null;
  public domain_name=null;
  public colleges=null;
  public college_name=null;
  public courses=null;
  public form = {
  	title: null,
  	domain: null,
  	course:null,
  	college:null,
  	text:null,
  	file:null,
  	city:null,
  	type:null
  }

  public addCourse=false;
  public addDomain=false;
  public addCollege=false;


  constructor(
  	private Jarwis: JarwisService,
  	private User: UserService,
  	private router:Router,

  	) { }

  ngOnInit() {
  	this.Jarwis.getAllColleges().subscribe(
  		data => this.handleCollegesResponse(data),
  		error => this.handleCollegesError(error)
  	);

  }

  inputFileClick(){
  	let el= this.fileInput.nativeElement;
  	el.click();
  }

  onSubmit(){
  	let formData = new FormData();
  	if(this.college_name == null){
 		this.college_name = this.form.college;
  	}
  	if(this.domain_name == null){
 		this.domain_name = this.form.domain;
  	} 
  	formData.append('title', this.form.title); 
  	formData.append('domain', this.domain_name); 
  	formData.append('course', this.form.course); 
  	formData.append('college', this.college_name); 
  	formData.append('text', this.form.text); 
  	formData.append('city', this.form.city);
  	formData.append('type', this.form.type); 
  	formData.append('user_id', this.User.getCurrentUserId().toString()); 
  	formData.append('file', this.fileInput.nativeElement.files[0], this.fileInput.nativeElement.files[0].name); 

  	this.Jarwis.addPost(formData).subscribe(
  		data => this.handleResponse(data),
  		error => this.handleError(error)
  	);
  }

  handleResponse(data){
    this.id = data.data[0].id;
  	this.router.navigateByUrl('/post/'+this.id);
  }

  handleError(error){
  	this.error = error.error.error;
  }

  handleDomainsResponse(data){
  	this.domains = data.data;
  }

  handleDomainsError(error){
  	this.error = error.error.error;
  }
  handleCollegesResponse(data){
  	this.colleges = data.data;
  }

  handleCollegesError(error){
  	this.error = error.error.error;
  }
  handleCoursesResponse(data){
  	this.courses = data.data;
  }

  handleCoursesError(error){
  	this.error = error.error.error;
  }

  onCollegeChange(data) {
  	this.form.city = data.value.city;
  	this.college_name = data.value.name;
    this.Jarwis.getDomainsByCollegeId(data.value.id).subscribe(
  		data => this.handleDomainsResponse(data),
  		error => this.handleDomainsError(error)
  	);
  }

  onDomainChange(data) {
  	this.domain_name = data.value.name;
    this.Jarwis.getCoursesByDomainId(data.value.id).subscribe(
  		data => this.handleCoursesResponse(data),
  		error => this.handleCoursesError(error)
  	);
  }

  addNewCollege(){
  	this.addCollege = !this.addCollege;
  	this.checkboxCourse._checked = !this.checkboxCourse._checked;
  	this.addCourse = !this.addCourse;
  	this.checkboxDomain._checked = !this.checkboxDomain._checked;
  	this.addDomain = !this.addDomain;


  }

  addNewDomain(){
  	this.addDomain = !this.addDomain;
  	this.checkboxCourse._checked = !this.checkboxCourse._checked;
  	this.addCourse = !this.addCourse;

  }



   	  	



}
