import {HttpClient} from '@angular/common/http';
import {Component, ViewChild, AfterViewInit, NgModule, OnInit} from '@angular/core';
import {MatPaginator, MatSort, MatTableModule, MatTableDataSource } from '@angular/material';
import {merge, Observable, of as observableOf} from 'rxjs';
import {catchError, map, startWith, switchMap} from 'rxjs/operators';
import { JarwisService } from '../../services/jarwis.service';
import { ActivatedRoute } from '@angular/router';




export interface PostData {
    created_at: string;
    title:string;
    domain: string;
    course: string;
    text: string;
    user:string;
}

@NgModule({
    imports: [MatPaginator, MatSort, MatTableModule, MatTableDataSource]
})

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements AfterViewInit {

   private error = null;
   private posts = null;

  displayedColumns: string[] = ['created_at','title','domain','course','text','user'];
  dataSource: MatTableDataSource<PostData>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private Jarwis:JarwisService,private route:ActivatedRoute) {
    if(this.route.snapshot.params['id']){
       this.posts = this.getUserPosts(this.route.snapshot.params['id']);
    }else{
       this.posts = this.getAllPosts();

    }


  }

  ngAfterViewInit() {

  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getAllPosts(){
      this.Jarwis.getAllPosts().subscribe(
          data => {
              this.handleResponse(data);
              this.dataSource.paginator = this.paginator;
              this.dataSource.sort = this.sort;
          },
          error => this.handleError(error)
      );
  }

    getUserPosts(id){
      this.Jarwis.getUserPosts(id).subscribe(
          data => {
              this.handleResponse(data);
              this.dataSource.paginator = this.paginator;
              this.dataSource.sort = this.sort;
          },
          error => this.handleError(error)
      );
  }

  handleResponse(data){

      this.dataSource = new MatTableDataSource(data.data);
  }

  handleError(error){
      this.error = error.error.error;
  }

}


