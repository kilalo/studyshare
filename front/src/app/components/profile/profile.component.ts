import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../../services/user.service';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {



  private users;
  private lol=1;

  constructor(private userService: UserService, private route:ActivatedRoute) { }

  ngOnInit() {
  	const id = this.route.snapshot.params['id'];
  	this.userService.getUserById(+id).subscribe(data => this.users = data);
  }

  getPostLike(){
  	let score = (this.users.data[0].post_likes/this.users.data[0].score)*100;
  	return score + ',100';
  }
  getCommentLike(){
  	let score = (this.users.data[0].comment_likes/this.users.data[0].score)*100;
  	return score + ',100';
  }

  getPostCount(){
  	let score = (this.users.data[0].post_count/this.users.data[0].score)*100;
  	return score + ',100';
  }

  getCommentCount(){
  	let score = (this.users.data[0].comment_count/this.users.data[0].score)*100;
  	return score + ',100';
  }

}
