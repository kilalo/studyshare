    import { Component, OnInit, NgModule } from '@angular/core';
import { Router } from '@angular/router';
import { JarwisService } from '../../services/jarwis.service';
import { TokenService } from '../../services/token.service';
import { AuthService } from '../../services/auth.service';
import { UserService } from '../../services/user.service';
import { MatInputModule, MatButtonModule } from '@angular/material';
import { NavbarComponent } from '../navbar/navbar.component';


@NgModule({
  imports: [MatInputModule, MatButtonModule]
})

@Component({
  providers:[NavbarComponent ],
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public form = {
  	email: null,
  	password: null
  }

  private id = null; 

  public error=null;
  constructor(
  	private Jarwis:JarwisService,
  	private Token:TokenService,
    private User:UserService,
  	private router:Router,
  	private Auth:AuthService,
    private navbar:NavbarComponent
  	) { }

  onSubmit(){
  	this.Jarwis.login(this.form).subscribe(
  		data => this.handleResponse(data),
  		error => this.handleError(error)
  	);
  }

  handleResponse(data){
  	this.Token.handle(data.access_token);
    this.User.handle(data.id);
    this.navbar.ngOnInit();
  	this.Auth.changeAuthStatus(true);
  	this.router.navigateByUrl('/home');
  	this.reload();
    window.location.href = window.location.origin + '/home';
    //this.reload();
  }

  reload(){

     window.location.reload();
     // this.router.navigate(['/home']);
  }
  handleError(error){
  	this.error = error.error.error;
  }
  ngOnInit() {
  }

}
