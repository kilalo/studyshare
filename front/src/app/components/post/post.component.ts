import { Component, OnInit, NgModule,ViewChild, Renderer2,AfterViewInit, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PostService } from '../../services/post.service';
import { UserService } from '../../services/user.service';
import { JarwisService } from '../../services/jarwis.service';
import { MatCard, MatButtonModule } from '@angular/material';
import { CommentComponent } from '../comment/comment.component';


@NgModule({
    imports: [MatCard, MatButtonModule],
    declarations: [PostComponent, CommentComponent],
})
@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  @ViewChild('comment_block') comment_block;
  @ViewChild('fileInput') fileInput;
  @ViewChild('comment') comment;
  @ViewChild('likeCount') likeCount;
  @ViewChild('likeButton')likeButton;
  @ViewChild('dislikeButton') dislikeButton;


	private posts;
  private error=null;
  private id: number;

  public form = {
  	text:null,
  	file:null,
  	user_id:null,
  	post_id:null
  };

  constructor(
  	private route:ActivatedRoute,
  	private Post:PostService,
  	private User:UserService,
  	private Jarwis:JarwisService,
    private renderer:Renderer2,
    private element:ElementRef
  	) { }

  ngOnInit() {
  	const id = this.route.snapshot.params['id'];
  	this.Post.getPostById(+id).subscribe(data => this.posts = data);
  }

  ngAfterViewInit(){
    let id = this.route.snapshot.params['id'];
    let user_id= this.User.getCurrentUserId().toString();
    let formData = new FormData(); 
    formData.append('post_id', id); 
    formData.append('user_id', user_id); 
    this.Jarwis.isPostLiked(formData).subscribe(
      data => this.handleIsPostLikedResponse(data)
    );
  }

  inputFileClick(){
  	let el= this.fileInput.nativeElement;
  	el.click();
  }

  onSubmit(){
  	let formData = new FormData(); 
  	this.form.post_id = this.route.snapshot.params['id'];
   	formData.append('post_id', this.form.post_id); 
  	formData.append('text', this.form.text);
  	formData.append('user_id', this.User.getCurrentUserId().toString());
  	if(this.fileInput.nativeElement.files[0]){
  	  formData.append('file', this.fileInput.nativeElement.files[0], this.fileInput.nativeElement.files[0].name); 
  	}

  	this.Jarwis.addComment(formData).subscribe(
  		data => this.handleResponse(data),
  		error => this.handleError(error)
  	);
  }

  handleResponse(data){
  	this.form.text = '';
  	this.fileInput.name = '';
    this.ngOnInit();
    this.id = data.data[0].name;
  }

  handleError(error){
  	this.error = error.error.error;
  }

  formatDate(date){
  	date = new Date(date);
  	var dd = date.getDate();

	var mm = date.getMonth()+1; 
	var yyyy = date.getFullYear();
	if(dd<10) 
	{
	    dd='0'+dd;
	} 

	if(mm<10) 
	{
	    mm='0'+mm;
	} 

	date = dd+'/'+mm+'/'+yyyy;
	return date;
  }

  like(data){
	  let formData = new FormData();
	  formData.append('post_id', this.posts.data[0].id);
	  formData.append('user_id', this.User.getCurrentUserId().toString() );
    let count = 0;
    if(data == '1'){
        if(this.dislikeButton._elementRef.nativeElement.classList.contains('liked')){
            this.renderer.removeClass(this.dislikeButton._elementRef.nativeElement,'liked');
            this.renderer.addClass(this.likeButton._elementRef.nativeElement,'liked');
            count = 2;
            data = 1;
        }else if(this.likeButton._elementRef.nativeElement.classList.contains('liked')){
            this.renderer.removeClass(this.likeButton._elementRef.nativeElement,'liked');
            count = -1;
            data = 0;
        }else{
            this.renderer.addClass(this.likeButton._elementRef.nativeElement,'liked');
            count = 1;
            data = 1;
        }
    }else{
        if(this.likeButton._elementRef.nativeElement.classList.contains('liked')){
            this.renderer.removeClass(this.likeButton._elementRef.nativeElement,'liked');
            this.renderer.addClass(this.dislikeButton._elementRef.nativeElement,'liked');
            data = -1;
            count = -2;
        }else if(this.dislikeButton._elementRef.nativeElement.classList.contains('liked')){
            data = 0;
            count = 1;
            this.renderer.removeClass(this.dislikeButton._elementRef.nativeElement,'liked');
        }else{
            data = -1;
            count = -1;
            this.renderer.addClass(this.dislikeButton._elementRef.nativeElement,'liked');
        }
    }
	  formData.append('like', data);
    formData.append('count_like', count.toString());

    var like = parseInt(this.likeCount.nativeElement.innerHTML) + count;
    this.likeCount.nativeElement.innerHTML = like;

	  this.Jarwis.addLikePost(formData).subscribe(
		  data => data
	  );
  }


  handleIsPostLikedResponse(data){
    if(data){
      const id = this.route.snapshot.params['id'];
      let user_id= this.User.getCurrentUserId().toString();
      let formData = new FormData(); 
      formData.append('post_id', id); 
      formData.append('user_id', user_id); 
      this.Jarwis.getLikeStatus(formData).subscribe(
          data => this.handleLikeStatusResponse(data)
        );
      }
    }

  handleLikeStatusResponse(data){
     let count = data;
     if(count == 1){
       this.renderer.addClass(this.likeButton._elementRef.nativeElement,'liked');
     }else if(count == -1){
       this.renderer.addClass(this.dislikeButton._elementRef.nativeElement,'liked');
     }
  }

}
