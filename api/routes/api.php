<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group([

    'middleware' => 'api',

], function ($router) {

    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');
    Route::post('logout', 'AuthController@logout');
    // Route::post('refresh', 'AuthController@refresh');
    // Route::post('me', 'AuthController@me');
    Route::post('sendPasswordResetLink', 'ResetPasswordController@sendEmail');
    Route::post('resetPassword', 'ChangePasswordController@process');

	Route::get('users', 'UserController@index');
	Route::get('users/{n}', 'UserController@index_one'); //Int user_id
	Route::post('users/register', 'UserController@store'); //Request(forename, name, domain, birth_date, email, password)
	Route::post('users/update', 'UserController@update'); //Request(user_id, forename, name, birth_date, domain_id)
	Route::post('users/delete', 'UserController@delete'); //Request(user_id)
	Route::get('users/{n}/like/posts', 'UserController@get_post_likes'); //Int user_id
	Route::get('users/{n}/like/comments', 'UserController@get_comment_likes'); //Int user_id
	Route::get('users/{n}/like/all', 'UserController@get_all_likes'); //Int user_id

	Route::get('posts/{n}', 'PostController@index_one');
	Route::get('posts/get_user/{n}', 'PostController@get_user_posts'); //Int post_id
	Route::get('posts/get_course/{n}', 'PostController@get_course_posts'); //Int user_id
	Route::get('posts/get_domain/{n}', 'PostController@get_domain_posts'); //Int domain_id
	Route::post('posts/register', 'PostController@store'); //Request(user_id, course_id, text)
	Route::post('posts/delete', 'PostController@delete'); //Request(post_id)
	Route::post('posts/like', 'PostController@like'); //Request(post_id, like)
	Route::post('posts/update', 'PostController@update'); //Request(post_id, text)
	Route::get('posts', 'PostController@get_posts');
	Route::get('tableposts', 'PostController@get_posts_table');
	Route::post('posts/search', 'PostController@search');
	Route::get('posts/{n}/comments', 'PostController@comments');

	Route::get('colleges/{n}', 'CollegeController@index_one'); //Int college_id
	Route::get('colleges', 'CollegeController@index');
	Route::post('colleges/register', 'CollegeController@store'); //Request(name, city)
	Route::post('colleges/delete', 'CollegeController@delete'); //Request(college_id)
	Route::post('colleges/update', 'CollegeController@update'); //Request(college_id, name)
    Route::get('colleges', 'CollegeController@index');

	Route::get('comments/{n}', 'CommentsController@index_one'); //Int comment_id
	Route::post('comments/register', 'CommentsController@store'); //Request(user_id, post_id, text)
	Route::post('comments/delete', 'CommentsController@delete'); //Request(comment_id)
	Route::post('comments/update', 'CommentsController@update'); //Request(comment_id, text)
    Route::post('comments/like', 'CommentsController@like'); //Request(comment_id, like)

	Route::get('courses/{n}', 'CourseController@index_one'); //Int course_id
	Route::get('courses/domains/{n}', 'CourseController@index_by_domain_id'); //Int domain_id
	Route::post('courses/register', 'CourseController@store'); //Request(user_id, post_id, text)
	Route::post('courses/delete', 'CourseController@delete'); //Request(comment_id)
	Route::post('courses/update', 'CourseController@update'); //Request(comment_id, text)
    Route::get('courses', 'CourseController@index');

	Route::get('domains/{n}', 'DomainController@index_one'); //Int domain_id
	Route::get('domains/colleges/{n}', 'DomainController@index_by_college_id'); //Int domain_id
	Route::post('domains/register', 'DomainController@store'); //Request(college_id, name)
	Route::post('domains/delete', 'DomainController@delete'); //Request(domain_id)
	Route::post('domains/update', 'DomainController@update'); //Request(domain_id, college_id, name)
    Route::get('domains', 'DomainController@index');

	Route::get('files/{n}', 'FileController@index_one'); //Int file_id
	Route::post('files/register', 'FileController@store'); //Request(name, type, file)
	Route::post('files/delete', 'FileController@delete'); //Request(file_id)

	Route::post('likes/is_comment_liked', 'LikeController@has_liked_comment');
	Route::post('likes/is_post_liked', 'LikeController@has_liked_post');
	Route::post('likes/update_status', 'LikeController@update_status');
    Route::post('likes/get_status', 'LikeController@get_status');

});