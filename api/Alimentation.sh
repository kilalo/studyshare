#!/bin/bash
#Vérifie que l'on a bien précisé un répertoire contenant des images
#if [ -z "$1" ]
#then
      #echo "Veuillez renseigner un dossier. "
      #exit 1
#fi

#img=$1$(ls "$1" | shuf -n 1)

#Enregistrements de posts
#curl --request POST \
  #--url http://api-study-share.com/api/posts/register \
  #--header 'Content-Type: application/x-www-form-urlencoded' \
  #--header 'Postman-Token: 46a3f371-c89e-4223-9a3e-e31203b798c4' \
  #--header 'cache-control: no-cache' \
  #--header 'content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' \
  #--form user_id=1 \
  #--form course_id=1 \
  #--form 'title=Corrigé génial' \
  #--form 'text=En fait c'\''est de la merde' \
  #--form "file=@$img" \
  #--form type=Annale \
  #--form 'college=IUT de Bordeaux' \
  #--form city=Bordeaux \
  #--form domain=MIAGE \
  #--form course=POO

#Enregistrements de courses
curl --request POST \
  --url http://api-study-share.com/api/courses/register \
  --header 'Content-Type: application/x-www-form-urlencoded' \
  --header 'Postman-Token: 6b58d65e-5b4d-4d0e-afbd-8ea47ddbd7ff' \
  --header 'cache-control: no-cache' \
  --data 'domain_id=1&name=POO'
  
curl --request POST \
  --url http://api-study-share.com/api/courses/register \
  --header 'Content-Type: application/x-www-form-urlencoded' \
  --header 'Postman-Token: 6b58d65e-5b4d-4d0e-afbd-8ea47ddbd7ff' \
  --header 'cache-control: no-cache' \
  --data 'domain_id=1&name=Finance'
  
curl --request POST \
  --url http://api-study-share.com/api/courses/register \
  --header 'Content-Type: application/x-www-form-urlencoded' \
  --header 'Postman-Token: 6b58d65e-5b4d-4d0e-afbd-8ea47ddbd7ff' \
  --header 'cache-control: no-cache' \
  --data 'domain_id=2&name=POO'
  
#Enregistrements de domains
curl --request POST \
  --url http://api-study-share.com/api/domains/register \
  --header 'Content-Type: application/x-www-form-urlencoded' \
  --header 'Postman-Token: 741dbada-b3c8-4f49-8071-aae7bb7d8474' \
  --header 'cache-control: no-cache' \
  --data 'college_id=1&name=MIAGE'

curl --request POST \
  --url http://api-study-share.com/api/domains/register \
  --header 'Content-Type: application/x-www-form-urlencoded' \
  --header 'Postman-Token: 741dbada-b3c8-4f49-8071-aae7bb7d8474' \
  --header 'cache-control: no-cache' \
  --data 'college_id=1&name=Biologie'

#Enregistrements de college
curl --request POST \
  --url http://api-study-share.com/api/colleges/register \
  --header 'Content-Type: application/x-www-form-urlencoded' \
  --header 'Postman-Token: 2dd0dc5a-5b3a-4d6f-80ff-6e9f23712363' \
  --header 'cache-control: no-cache' \
  --data 'name=Universit%C3%A9%20de%20Bordeaux&city=Bordeaux'

#Enregistrements d'utilisateurs
curl --request POST \
  --url http://api-study-share.com/api/users/register \
  --header 'Content-Type: application/x-www-form-urlencoded' \
  --header 'Postman-Token: 684f9185-96a4-4832-a9f0-121b095f3701' \
  --header 'cache-control: no-cache' \
  --data 'forename=Jean&name=Bombeur&domain=1&birth_date=1996-05-20&email=jean.bombeur%40zgeg.io&password=pwd&undefined='

curl --request POST \
  --url http://api-study-share.com/api/users/register \
  --header 'Content-Type: application/x-www-form-urlencoded' \
  --header 'Postman-Token: 684f9185-96a4-4832-a9f0-121b095f3701' \
  --header 'cache-control: no-cache' \
  --data 'forename=Pierre&name=Abron&domain=1&birth_date=1996-12-01&email=pierre.abron%40gmail.com&password=pwd&undefined='

curl --request POST \
  --url http://api-study-share.com/api/users/register \
  --header 'Content-Type: application/x-www-form-urlencoded' \
  --header 'Postman-Token: 684f9185-96a4-4832-a9f0-121b095f3701' \
  --header 'cache-control: no-cache' \
  --data 'forename=Octave-Hergé&name=Bel&domain=1&birth_date=1969-06-09&email=oct.avo%40wanadoo.fr&password=pwd&undefined='

curl --request POST \
  --url http://api-study-share.com/api/users/register \
  --header 'Content-Type: application/x-www-form-urlencoded' \
  --header 'Postman-Token: 684f9185-96a4-4832-a9f0-121b095f3701' \
  --header 'cache-control: no-cache' \
  --data 'forename=Jean-Michel&name=Belami&domain=1&birth_date=1985-04-21&email=street.zer%40yahoo.com&password=pwd&undefined='