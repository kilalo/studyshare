@component('mail::message')
# Procédure d'oubli de mot de passe

Pour réinitialiser votre mot de passe, veuillez cliquer sur le bouton ci-dessous.

@component('mail::button', ['url' => 'http://localhost:4200/response-password-reset?token=' . $token->token ])
Réinitialiser le mot de passe
@endcomponent

L'équipe StudyShare,<br>
{{ config('app.name') }}
@endcomponent
