<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\User;
use App\Course;
use App\Comments;
use App\File;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = User::get()->where('id', $this->user_id);
        $course = Course::get()->where('id', $this->course_id);
        $comment = Comments::get()->where('post_id',$this->id);
        $file = File::get()->where('id',$this->file_id);
        return [
            'id'=>$this->id,
            'user'=>UserResource::collection($user),
            'course'=>CourseResource::collection($course),
            'comment'=>CommentsResource::collection($comment),
            'file'=>FileResource::collection($file),
            'like_count'=>$this->like_count,
            'title'=>$this->title,
            'text'=>$this->text,
            'created_at'=>$this->created_at,
            'updated_at'=>$this->updated_at
        ];
    }
}
