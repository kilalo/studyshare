<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Controllers\UserController;
use App\Domain;


class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $domain = Domain::get()->where('id', $this->domain_id);

        return [
            'id'=>$this->id,
            'name'=>$this->name,
            'forename'=>$this->forename,
            'email'=>$this->email,
            'birth_date'=>$this->birth_date,
            'is_admin'=>$this->is_admin,
            'domain'=>DomainResource::collection($domain),
            'post_likes' => (new UserController)->get_post_likes($this->id),
            'post_count' => (new UserController)->get_post_count($this->id),
            'comment_likes' => (new UserController)->get_comment_likes($this->id),
            'comment_count' => (new UserController)->get_comment_count($this->id),
            'score'=> (new UserController)->get_score($this->id),
            'created_at'=>$this->created_at,
            'updated_at'=>$this->updated_at
        ];
    }
}
