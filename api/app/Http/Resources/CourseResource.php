<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Domain;

class CourseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $domain = Domain::get()->where('id', $this->domain_id);
        return [
            'id'=>$this->id,
            'domain'=>DomainResource::collection($domain),
            'name'=>$this->name,
            'created_at'=>$this->created_at,
            'updated_at'=>$this->updated_at
        ];
    }
}
