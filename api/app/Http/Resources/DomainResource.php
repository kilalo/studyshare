<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\College;

class DomainResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $college = College::get()->where('id', $this->college_id);
        return [
            'id'=>$this->id,
            'college'=>CollegeResource::collection($college),
            'name'=>$this->name,
            'created_at'=>$this->created_at,
            'updated_at'=>$this->updated_at
        ];
    }
}
