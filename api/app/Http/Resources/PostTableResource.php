<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\User;
use App\Course;
use App\Comments;
use App\Domain;
use App\File;

class PostTableResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user_forename = User::where('id', $this->user_id)->pluck('forename');
        $user_name = User::where('id', $this->user_id)->pluck('name');
        $user_id = User::where('id', $this->user_id)->pluck('id');
        $user = $user_forename[0] . ' ' . $user_name[0];
        $course = Course::get()->where('id', $this->course_id)->pluck('name');
        $domain_id = Course::get()->where('id', $this->course_id)->pluck('domain_id');
        $file = File::get()->where('id',$this->file_id)->pluck('name');
        $domain = Domain::get()->where('id', $domain_id[0])->pluck('name');


        return [
            'user'=>$user,
            'course'=>$course[0],
            'text' => $this->text,
            'title' => $this->title,
            'created_at' => $this->created_at,
            'domain' => $domain[0],
            'user_id' => $user_id[0],
            'post_id' => $this->id
        ];
    }
}
