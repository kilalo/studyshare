<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\User;
use App\Course;
use App\File;


class CommentsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = User::get()->where('id', $this->user_id);
        $course = Course::get()->where('id', $this->course_id);
        $file = File::get()->where('id',$this->file_id);
        return [
            'id'=>$this->id,
            'user'=>UserResource::collection($user),
            'post'=>PostResource::collection($course),
            'text'=>$this->text,
            'file'=>FileResource::collection($file),
            'like_count'=>$this->like_count,
            'created_at'=>$this->created_at,
            'updated_at'=>$this->updated_at
        ];
        //return parent::toArray($request);
    }
}
