<?php

namespace App\Http\Controllers;

use App\File;
use App\Http\Resources\FileResource;
use Illuminate\Http\Request;

class FileController extends Controller
{
    public function store(Request $request){
        $file = $request->file;
        $name = $file->getClientOriginalName(); //Le nom original du fichier
        $type = $request->type;

        $filre_ret = File::create([
            'name' => $name,
            'type' => $type
        ]);

        //$file->move('storage/upload',$name);
        $file->store('storage/upload');
        $file->move('storage/upload');

        return (FileResource::collection(File::get()->where('id', '=', $file_ret->id)));
    }

    public function index_one(Int $file_id){
        $file = File::get()->where('id', '=', $file_id);
        return (FileResource::collection($file));
    }

    public function delete(Request $request){
        $file = File::where('id', $request->file_id)->first();
        $file->delete();
    }

}
