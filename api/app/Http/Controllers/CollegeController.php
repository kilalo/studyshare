<?php

namespace App\Http\Controllers;

use App\Http\Resources\CollegeResource;
use App\College;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CollegeController extends Controller
{
    public function store(Request $request){

        $name = $request->name;
        $city = $request->city;

        if (DB::table('colleges') //On regarde si le college n'existe pas déjà
            ->where('name', '=', $name)
            ->where('city', '=', $city)
            ->exists()){

            return DB::table('colleges')->where('name', '=', $name)->where('city', '=', $city)->get();

        }else {
            $college = College::create([
                'name' => $name,
                'city' => $city
            ]);

            return DB::table('colleges')->where('name', '=', $name)->where('city', '=', $city)->get();
        }

    }

    public function update(Request $request){
        $college = Colleges::where('id', $request->college_id)->first();
        $college->name = $request->name;
        $college->save();
    }

    public function index_one(String $college_id){
        $college = College::get()->where('id', '=', $college_id);
        return (CollegeResource::collection($college));
    }

    public function index(){
        $college = College::orderBy('name')->get();
        return (CollegeResource::collection($college));
    }

    public function delete(Request $request){
        $college = College::where('id', $request->college_id)->first();
        $college->delete();
    }

}
