<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Auth\RegisterController;
use App\Http\Resources\UserResource;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function get_form(){
    	return view('User');
    }

    public function post_form(UserRequest $request){
    	$user = new User;
    	$user->user = $request->input('user');
    	$user->save();

    	return view('user_ok');
    }

    public function store(Request $request){
    	$forename = $request->input('forename');
        $name = $request->input('name');
        $domain = $request->input('domain');
        $birth_date = $request->birth_date;
        $email = $request->email;
        $password = $request->password;

        $user = User::create([
        	'forename' => $forename,
            'name' => $name,
            'email' => $email,
            'domain_id' => $domain,
            'birth_date' => $birth_date,
            'password' => $password
        ]);

        return (UserResource::collection(User::get()->where('id', '=', $user->id)));
    }

    public function update(Request $request){
        $user = User::where('id', $request->user_id)->first();
        $user->forename = $request->input('forename');
        $user->name = $request->input('name');
        $user->birth_date = $request->birth_date;
        $user->domain_id = $request->domain_id;
        $user->save();
    }

    public function delete(Request $request){
        $user = User::where('id', $request->user_id)->first();
        $user->delete();
    }

    public function index(){
    	$users = User::get();
        return (UserResource::collection($users));
    }

    public function index_one(Int $user_id){
    	$users = User::get()->where('id', '=', $user_id);
        return (UserResource::collection($users));
    }

    public function get_post_likes(Int $user_id){
        $number_like = DB::table('posts')
            ->where('user_id', '=', $user_id )
            ->sum('like_count');

        return (Int) $number_like;
    }

    public function get_post_count(Int $user_id){
        $post_count = DB::table('posts')
            ->where('user_id', '=', $user_id )
            ->count();

        return (Int) $post_count;
    }

    public function get_comment_likes(Int $user_id){
        $number_like = DB::table('comments')
            ->where('user_id', '=', $user_id )
            ->sum('like_count');

        return (Int) $number_like;
    }

    public function get_comment_count(Int $user_id){
        $comment_count = DB::table('comments')
            ->where('user_id', '=', $user_id )
            ->count();

        return (Int) $comment_count;
    }


    public function get_all_likes(Int $user_id){
        return ($this->get_post_likes($user_id) + $this->get_comment_likes($user_id));
    }

    public function get_score(Int $user_id){
        return ($this->get_post_likes($user_id) + $this->get_comment_likes($user_id) + $this->get_post_count($user_id) + $this->get_comment_count($user_id));

    }
}
