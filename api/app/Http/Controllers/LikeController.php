<?php

namespace App\Http\Controllers;

use App\Like;
use Illuminate\Http\Request;

class LikeController extends Controller
{

    public function has_liked_comment(Request $request){
        $user_id = $request->user_id;
        $comment_id = $request->comment_id;
        $test = Like::where('user_id' ,'=', $user_id)
            ->where('comment_id', '=', $comment_id)
            ->exists();
        return response()->json($test);
    }

    public function has_liked_post(Request $request){
        $user_id = $request->user_id;
        $post_id = $request->post_id;
        $test = Like::where('user_id' ,'=', $user_id)
            ->where('post_id', '=', $post_id)
            ->exists();
        return response()->json($test);
    }

    public function update_status(Request $request){
        $like = Like::where('id', $request->like_id)->first();
        $like->status = $request->status;
        $like->save();
    }

    public function get_status(Request $request){
        $user_id = $request->user_id;
        if($request->post_id != ''){
            $post_id = $request->post_id;
            $status = Like::where('user_id' ,'=', $user_id)
                ->where('post_id', '=', $post_id)->pluck('status');
        }

        if($request->comment_id != ''){
            $comment_id = $request->comment_id;
            $status = Like::where('user_id' ,'=', $user_id)
                ->where('comment_id', '=', $comment_id)->pluck('status');
        }
        //var_dump($request->post_id);

        return $status;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
