<?php

namespace App\Http\Controllers;

use App\Domain;
use App\Http\Resources\DomainResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DomainController extends Controller
{
    public function store(Request $request){
        $name = $request->name;
        $college_id = $request->college_id;

        if (DB::table('domains') //On regarde si le domain n'existe pas déjà
            ->where('name', '=', $name)
            ->where('college_id', '=', $college_id)
            ->exists()){
            return DB::table('domains')->where('name', '=', $name)->where('college_id', '=', $college_id)->get();
        }else {
            $domain = Domain::create([
                'name' => $name,
                'college_id' => $college_id
            ]);
            
            return DB::table('domains')->where('name', '=', $name)->where('college_id', '=', $college_id)->get();
        }
    }

    public function index_one(Int $domain_id){
        $domain = Domain::get()->where('id', '=', $domain_id);
        return (DomainResource::collection($domain));
    }

    public function index_by_college_id(Int $college_id){
        $domain = Domain::where('college_id', '=', $college_id)->orderBy('name')->get();
        return (DomainResource::collection($domain));
    }

    public function index(){
        $domain = Domain::get();
        return (DomainResource::collection($domain));
    }

    public function update(Request $request){
        $domain = Posts::where('id', $request->domain_id)->first();
        $domain->college_id = $request->college_id;
        $domain->name = $request->name;
        $domain->save();
    }

    public function delete(Request $request){
        $domain = Domain::where('id', $request->domain_id)->first();
        $domain->delete();
    }

}
