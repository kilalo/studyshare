<?php

namespace App\Http\Controllers;

use App\Comments;
use App\Http\Resources\CommentsResource;
use App\File;
use App\Post;
use App\Like;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    public function store(Request $request){
        $user_id = $request->user_id;
        $post_id = $request->post_id;
        $text = $request->text;

        $type = $request->type;
        $file = $request->file;

        if (is_null($file)){
            $file_id = null;
        }
        else{
            $file->store('storage/upload');
            $name = $file->hashName();
            $file->move('storage/upload',$name);

            $file = File::create([
                'name' => $name,
                'type' => 'Comment'
            ]);
            $file_id = $file->id;
        }

        $comment = Comments::create([
            'user_id' => $user_id,
            'post_id' => $post_id,
            'file_id' => $file_id,
            'text' => $text,
            'like_count' => '0'
        ]);

        return (CommentsResource::collection(Comments::get()->where('id', '=', $comment->id)));
    }

    public function index_one(Int $comments_id){
        $comments = Comments::get()->where('id', '=', $comments_id);
        return (CommentsResource::collection($comments));
    }

    public function update(Request $request){
        $comment = Comments::where('id', $request->comment_id)->first();
        $comment->text = $request->text;
        $comment->save();
    }

    public function like(Request $request){
        $comment = Comments::where('id' , $request->comment_id)->first();
        $comment->like_count += (int) $request->count_like;

        if (Like::where('comment_id', $request->comment_id)
            ->where('user_id', $request->user_id)
            ->exists()){
            $like = Like::where('comment_id', $request->comment_id)
            ->where('user_id', $request->user_id)
            ->first();
            $like->status = $request->like;
            $like->save();
        }else{
            Like::create([
                'user_id' => $request->user_id,
                'comment_id' => $request->comment_id,
                'status' => $request->like
            ]);
        }

        $comment->save();
    }

    public function delete(Request $request){
        $comment = Comments::where('id', $request->comment_id)->first();
        $comment->delete();
    }
}
