<?php

namespace App\Http\Controllers;

use App\Course;
use App\Http\Resources\CourseResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CourseController extends Controller
{
    public function store(Request $request){
        $name = $request->name;
        $domain_id = $request->domain_id;

        if (DB::table('courses') //On regarde si le course n'existe pas déjà
            ->where('name', '=', $name)
            ->where('domain_id', '=', $domain_id)
            ->exists()){

            return DB::table('courses')->where('name', '=', $name)->where('domain_id', '=', $domain_id)->get();

        }
        else {
            $course = Course::create([
                'name' => $name,
                'domain_id' => $domain_id
            ]);
                        
            return DB::table('courses')->where('name', '=', $name)->where('domain_id', '=', $domain_id)->get();

        }
    }

    public function update(Request $request){
        $course = Posts::where('id', $request->course_id)->first();
        $course->domain_id = $request->domain_id;
        $course->name = $request->name;
        $course->save();
    }

    public function index_by_domain_id(Int $domain_id){
        $course = Course::where('domain_id', '=', $domain_id)->orderBy('name')->get();
        return (CourseResource::collection($course));
    }

    public function index_one(Int $course_id){
        $course = Course::get()->where('id', '=', $course_id);
        return (CourseResource::collection($course));
    }

    public function index(){
        $course = Course::get();
        return (CourseResource::collection($course));
    }

    public function delete(Request $request){
        $course = Course::where('id', $request->course_id)->first();
        $course->delete();
    }

}
