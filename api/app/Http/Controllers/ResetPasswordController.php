<?php

namespace App\Http\Controllers;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\User;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Mail\ResetPasswordMail;

class ResetPasswordController extends Controller
{
    public function sendEmail(Request $request){
    	if(!($this->validateEmail($request->email))){
    		$this->failedResponse();
    	}

    	$this->send($request->email);

    	return $this->successResponse();
    }

    public function send($email){
    	$token = $this->createToken($email);
    	Mail::to($email)->send(new resetPasswordMail($token));
    }

    public function createToken($email){
    	$oldToken = DB::table('password_resets')->where('email',$email)->first();
    	
    	if($oldToken){
    		return $oldToken;
    	}

    	$token = str_random(60);
    	$this->saveToken($token,$email);

    	return $token;
    }

    public function saveToken($token,$email){
    	DB::table('password_resets')->insert([
			'email' => $email,
			'token' => $token,
			'created_at' => Carbon::now()

    	]);
    }

    public function validateEmail($email){
    	!!User::where('email',$email)->first();
    }

    public function failedResponse(){

    	return response()->json([
    		'error' => 'L\'adresse mail n\'existe pas'
    	], Response::HTTP_NOT_FOUND);

    }

    public function successResponse(){

    	return response()->json([
    		'data' => 'Un mail de réinitialisation vous a été envoyé avec succès !'
    	], Response::HTTP_OK);

    }
}
