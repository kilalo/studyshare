<?php

namespace App\Http\Controllers;

use App\Comments;
use App\Http\Resources\CommentsResource;
use App\User;
use Illuminate\Http\Request;
use App\Http\Resources\PostResource;
use App\Http\Resources\PostTableResource;
use App\Http\Resources\FileResource;
use App\Post;
use App\Course;
use App\Domain;
use App\College;
use App\File;
use App\Like;
use Illuminate\Support\Facades\DB;
use FileController;

class PostController extends Controller
{
	
	public function store(Request $request){
		$user_id = $request->user_id;
		$title = $request->input('title');
		$text = $request->input('text');
		$file = $request->file;
        $type = $request->type;
		$file->store('storage/upload');
        $name = $file->hashName();
        $file->move('storage/upload',$name);

        $request_college = new Request([
			'name' => $request->college,
        	'city' => $request->city
    	]);
        $json_college = app('App\Http\Controllers\CollegeController')->store($request_college);
        $json_college = json_decode($json_college);

        $request_domain = new Request([
		    'name' => $request->domain,
        	'college_id' => $json_college[0]->id
    	]);
        $json_domain = app('App\Http\Controllers\DomainController')->store($request_domain);
        $json_domain = json_decode($json_domain);

        $request_course = new Request([
			'name' => $request->course,
        	'domain_id' => $json_domain[0]->id
    	]);
        $json_course = app('App\Http\Controllers\CourseController')->store($request_course);
        $json_course = json_decode($json_course);
		$file = File::create([
            'name' => $name,
            'type' => $type
        ]);

        $post = Post::create([
			'user_id' => $user_id,
			'course_id' => $json_course[0]->id,
			'like_count' => 0,
			'file_id' => $file->id,
			'title' => $title,
			'text' => $text
		]);

		return (PostResource::collection(Post::get()->where('id', '=', $post->id)));
	}

	public function index_one(Int $post_id){
		$posts = Post::get()->where('id', '=', $post_id);
		return (PostResource::collection($posts));
	}

	public function update(Request $request){
        $post = Posts::where('id', $request->post_id)->first();
        $post->title = $request->title;
        $post->text = $request->text;
        $post->save();
    }

	public function get_user_posts(Int $user_id){
		$posts = Post::get()->where('user_id', '=', $user_id);
		return (PostTableResource::collection($posts));
	}

	public function get_course_posts(Int $course_id){
		$posts = Post::get()->where('course_id', '=', $course_id);
		return (PostResource::collection($posts));
	}

	public function get_domain_posts(Int $domain_id){
		$posts = DB::table('posts')
			->join('courses', 'course_id', '=', 'courses.id')
			->where('domain_id', '=', $domain_id)
			->get();
		return $posts;
	}

	public function like(Request $request){
	    $post = Post::where('id' , $request->post_id)->first();
	    $post->like_count += (int) $request->count_like;

        if (Like::where('post_id', $request->post_id)
            ->where('user_id', $request->user_id)
            ->exists()){
            $like = Like::where('post_id', $request->post_id)
            ->where('user_id', $request->user_id)
            ->first();
            $like->status = $request->like;
            $like->save();
        }else{
            Like::create([
                'user_id' => $request->user_id,
                'post_id' => $request->post_id,
                'status' => $request->like
            ]);
        }

	    $post->save();
    }

	public function delete(Request $request){
        $post = Post::where('id', $request->post_id)->first();
        $post->delete();
    }

    public function get_posts(){
	    $post = Post::get();
	    return(PostResource::collection(($post)));
    }

    public function get_posts_table(){
	    $post = Post::get();
	    return(PostTableResource::collection(($post)));
    }

    public function search (Request $request){

       $post = Post::join('courses', 'course_id', '=', 'courses.id')
            ->join('domains', 'domain_id', '=', 'domains.id')
            ->select('posts.*','courses.domain_id','courses.name','domains.college_id','domains.name as nm')
           ->get();
        //user
        if($request->user){
            $user = User::get()->where('name', $request->user)->first();
            $post = $post->where('user_id', $user->id);
        }

        if($request->domain){
            $domain = Domain::get()->where('name', $request->domain)->first();
            $post = $post->where('domain_id', $domain->id);
        }

        if($request->course){
            $course = Course::get()->where('name', $request->course)->first();
            $post = $post->where('course_id', $course->id);
        }

        if($request->college){
            $college = College::get()->where('name', $request->college)->first();
            $post = $post->where('college_id', $college->id);
        }
        return (PostResource::collection($post));
    }

    public function comments(Int $post_id){
	    $comments = Comments::get()->where('post_id', $post_id);
	    return (CommentsResource::collection($comments));
    }

}
