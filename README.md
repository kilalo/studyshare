Configuration pour le développement en local :

- Modifier le fichier httpd-vhosts.conf

~~~~
#### STUDYSHARE API ####

<VirtualHost *:80>
  ServerName api-study-share.com
  ServerAlias api-study-share.com
  DocumentRoot "C:/wamp64/www/StudyShare/api/public"
  <Directory "C:/wamp64/www/StudyShare/api/">
    Options +Indexes +Includes +FollowSymLinks +MultiViews
    AllowOverride All
    Require local
  </Directory>
  LogLevel warn
  ErrorLog "C:/wamp64/logs/api-study-share.error.log"
  CustomLog "C:/wamp64/logs/api-study-share.access.log" combined

</VirtualHost>

#### STUDYSHARE FRONT ####
<VirtualHost *:80>
  ServerName study-share.com
  ServerAlias study-share.com
  DocumentRoot "C:/wamp64/www/StudyShare/front/src"
  <Directory "C:/wamp64/www/StudyShare/front/">
    Options +Indexes +Includes +FollowSymLinks +MultiViews
    AllowOverride All
    Require local
  </Directory>
   LogLevel warn
  ErrorLog "C:/wamp64/logs/study-share.error.log"
  CustomLog "C:/wamp64/logs/study-share.access.log" combined
</VirtualHost>
~~~~

- Ajouter ces 2 lignes au fichier hosts

~~~
127.0.0.1 api-study-share.com
127.0.0.1 study-share.com
~~~

- Redémarrer Apache 

- Création de la base de données en local

Se connecter à MYSQL en local

~~~
$mysql -u root -p
~~~

Et créer la BD 

~~~
create database STUDY_SHARE_DB;
GRANT ALL PRIVILEGES ON STUDY_SHARE_DB.* TO 'STUDY_SHARE_USER'@'localhost' IDENTIFIED BY 'STUDY_SHARE_PASSWORD';
flush privileges;
~~~


Installation du projet :

FRONT :

- Installer les librairies

 ~~~
$npm i
~~~

- Lancer le développement local

 ~~~
$ng serve
~~~

API :

- Installer package JWTAuth

 ~~~
$php artisan vendor:publish --provider="Tymon\JWTAuth\Providers\LaravelServiceProvider.php"
$php artisan jwt:secret
~~~

